# KIV/DS - 1. Samostaná práce 
## *Ondřej Anděl*

### Popis řešení
Volba mastera probíhá upravenou verzí bully algoritmu, jednotlivé nody vždy 
po přihlášení pošlou přes udp broadcast zprávu začínající prefixem `new-` 
následované svým hostname. Všechny ostatní nody si při obdržení této zprávy
nový node přidají do setu, a porovnají, zda nové hostname je hodnotou větší než
hostname leadera. Každá nová stanice si na začátku svého chodu myslí, že je leaderem.
Další akcí každé stanice při obdržení danné zprávy je odpověď začínající prefixem
`add-` opět následované svým hostname. Při obdržení této zprávy si každá stanice upraví
svůj set nodů a zkontroluje, zda příchozí hostname není větší než hostname leadera. Zprávy
typu `new-` a `add-` se liší pouze tím, že na první typ ostatní stanice odpovídají.
Díky tomuto algoritmu mají všechny stanice seznam ostatních uzlů a zvoleného stejného leadera.
Volba je ukončena pokud během 30 sekund nepřijde žádná nová zpráva.

Po ukončení volby vzniklý leader opustí cyklus přijímání zpráv, zatímco zbylé nody
v témže cyklu zůstanou. Leader dále vypočte počet zelených stanic, daný výsledek nikterak
nezaokrouhluje. Dále je inicializován counter zelených nodů, jež je nastaven na hodnotu 1.
Leader dále nastaví sobě barvu na zelenou, a začne jednotlivým stanicím uložených v setu 
přidělovat postupně zelenou barvu (vyšle zprávu `grn-` následovanou příslušným hostnamem) 
a navyšovat counter obarvených nodů. Pokud byl již překročen předem vypočtený počet 
zelených uzlů, leader obarví zbylé zařízení červeně zprávou `red-` následovanou příslušnými hostname.
Po úspěšném obarvení všech uzlů vyšle leader zprávu `clr-` následovanou vlastním hostname.
Při obdržení této zprávy uzly vytisknou svojí barvu do konzole. 

### Popis spuštění
Aplikaci je možné spustit prostřednictvím příkazu `vagrant up`, za předpokladu,
že veškeré potřebné nástroje (vagrant, docker) jsou již nainstalovány a správně
nastaveny. Pro aplikaci je možné nastavit počet vytvářených uzlů sítě v souboru
`Vagrantfile` úpravou parametru `BACKENDS_COUNT`. Po spuštění programu dojde 
k nasazení uzlů sítě a spuštěný scriptu `main.py`. Při úspěšném chodu jsou 
do konzole tištěny výsledky volby (zvolený uzel oznámý "leader") a výsledky obavení,
kdy každá stanice vytiskne buď "green" nebo "red". V případě vytištění "init"
nejspíš nastala nějaká chyba v chodu programu, jelikož daná stanice nebyla obarvena 
(nebo to nezaznamenala).

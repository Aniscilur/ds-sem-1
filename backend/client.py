import socket

def start_client():
    color = "init"
    nodes = set()
    leader = socket.gethostname()
    print("starting node " + leader, flush=True)

    #init client
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    client.bind(("", 37020))
    client.settimeout(30)

    #init server
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    server.settimeout(0.2)

    #peasant and election loop
    own_id = socket.gethostname()
    msg_len = len(own_id) + 4
    server.sendto(bytes("new-"+own_id, 'utf-8'), ('<broadcast>', 37020))
    while True:
        try:
            data, addr = client.recvfrom(msg_len)
            msg = data.decode('UTF-8')
            if msg.startswith("grn-"):
                if msg.strip("grn-") == own_id:
                    color = "green"
            elif msg.startswith("red-"):
                if msg.strip("red-") == own_id:
                    color = "red"
            elif msg.startswith("clr-"):
                if msg.strip("clr-") == leader:
                    print(color, flush=True)
                    break
                else:
                    print("color request by peasant", flush=True)
            elif msg.startswith("new-"):
                new_ip = msg.strip("new-")
                server.sendto(bytes("add-"+own_id, 'utf-8'), ('<broadcast>', 37020))
                nodes.add(new_ip)
                if new_ip > leader:
                    leader = new_ip  
            elif msg.startswith("add-"):
                new_ip = msg.strip("add-")
                nodes.add(new_ip)
                if new_ip > leader:
                    leader = new_ip
        except socket.timeout:
            if leader == own_id:
                break
            else:
                continue
    
    #let the leader work begin
    if leader == own_id:
        print("leader", flush=True)
        color = "green"
        size = len(nodes)
        green_count = size / 3
        green_i = 1
        for node in nodes:
            if green_count > green_i:
                server.sendto(bytes("grn-"+node, 'utf-8'), ('<broadcast>', 37020))
            else:
                server.sendto(bytes("red-"+node, 'utf-8'), ('<broadcast>', 37020))
            green_i += 1
        
        server.sendto(bytes("clr-"+leader, 'utf-8'), ('<broadcast>', 37020))
        print(color, flush=True)